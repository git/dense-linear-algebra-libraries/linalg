Build instructions of LINALG component:

1.--------- Set environment variables ---------

Following environment variables must be set in order to build DSP-only LINALG (version numbers are used as examples):

export CGTROOT="<TI_CGT_INSTALLATION_ROOT>/cgt-c6x"
export PDK_DIR="<COMPONENTS_INSTALLATION_ROOT>/pdk_c667x_2_0_1"
export FC_DIR="<COMPONENTS_INSTALLATION_ROOT>/framework_components_3_40_01_04"
export XDAIS_DIR="<COMPONENTS_INSTALLATION_ROOT>/xdais_7_24_00_04"
export BIOS_DIR="<COMPONENTS_INSTALLATION_ROOT>/bios_6_45_01_29"
export OMP_DIR="<COMPONENTS_INSTALLATION_ROOT>/openmp_dsp_c667x_2_02_00_02"
export XDC_DIR="<COMPONENTS_INSTALLATION_ROOT>/xdctools_3_32_00_06_core"
export IPC_DIR="<COMPONENTS_INSTALLATION_ROOT>/ipc_3_42_00_02"
export EDMA3_DIR="<COMPONENTS_INSTALLATION_ROOT>/edma3_lld_02_12_01_23"
export LIBARCH_DIR="<COMPONENTS_INSTALLATION_ROOT>/libarch_1_0_0_0"
export PATH=<TI_CGT_INSTALLATION_ROOT>/cgt-c6x/bin:$PATH

Additional environment variables must be set to build ARM+DSP LINALG:
export TI_OCL_INSTALL_DIR="<PSDK_INSTALLATION_ROOT>/linux-devkit/sysroots/cortexa15hf-vfp-neon-linux-gnueabi/usr/share/ti/opencl"
export TI_OCL_CGT_INSTALL="<PSDK_INSTALLATION_ROOT>/linux-devkit/sysroots/x86_64-arago-linux/usr/share/ti/cgt-c6x"
export TARGET_ROOTDIR="<PSDK_INSTALLATION_ROOT>/linux-devkit/sysroots/cortexa15hf-vfp-neon-linux-gnueabi"

2.--------- Build LINALG source code and make it a RTSC package ---------

From the root folder of LINALG, the build command is:
make -f build/Makefile MEM_MODEL=<memory_model_name> TARGET=<target_name> LIBOS=<os_name>

where:
   1) <memory_model_name> must be one of Large, Medium, or Small
   2) <target_name> must be one of SOC_K2H, SOC_C6678, or SOC_AM572x
   3) <os_name> must be one of LIB_OPENCL or LIB_RTOS

Examples:
1) to build LINALG Large memory model for K2H in OpenCL environment, type:
make -f build/Makefile MEM_MODEL=Large TARGET=SOC_K2H LIBOS=LIB_OPENCL

2) to build LINALG Medium memory model for C6678 in RTOS environment, type:
make -f build/Makefile MEM_MODEL=Medium TARGET=SOC_C6678 LIBOS=LIB_RTOS

3.--------- Build LINALG examples ---------

1) Install the LINALG package generated in previous step by unzipping the package file in folder ./exports:
   - linalg_<version>.zip 
   
2) Add one more environment variable to point to LINALG's path:
export LINALG_DIR="<LINALG installation folder>"

3) Go to examples folder and use build command:
   1) For ARM+DSP: cd arm+dsp; make
   1) For DSP-only: cd dsponly; make TARGET=<target_name> LIBOS=LIB_RTOS
      where: <target_name> must be one of SOC_K2H, SOC_C6678, or SOC_AM572x

4.--------- Build BLIS test suite for ARM+DSP ---------
# BLIS ARM+DSP test suite Makefile links ARM+DSP lib and ARM BLIS lib from inside src folder
# BLIS_ARM_LIB_DIR = ../install/arm/lib
# BLIS_BLAS_ACC_LIB_DIR =  ../../blasblisacc/lib
# BLIS_LIB       := $(BLIS_BLAS_ACC_LIB_DIR)/libcblas_armplusdsp.a $(BLIS_ARM_LIB_DIR)/libblis.a -lOpenCL -locl_util -lstdc++ -lrt -lpthread 
# Use same environment variables as building the examles.
cd packages/ti/linalg
make ARMplusDSP MEM_MODEL=Large TARGET=SOC_K2H LIBOS=LIB_OPENCL
make BLIStest MEM_MODEL=Large TARGET=SOC_K2H LIBOS=LIB_OPENCL
The executalbe test_libblis_cortex-a15.x is located in packages/ti/linalg/blis/testsuite.

5.--------- Build BLIS test suite for DSP-only ---------
# BLIS DSP test suite shares same make process defined in examples/dsponly/common/Makefile.common. It searches include path and lib path
# in RTSC packaging style: $(LINALG_DIR)/packages/ti/linalg. Therefore, to build BLIS test suite, LINALG needs to be installed first.
# Use same environment variables as building the examles.
cd packages/ti/linalg
make DSPlibs MEM_MODEL=Medium TARGET=SOC_C6678 LIBOS=LIB_RTOS
make BLIStestDSP MEM_MODEL=Medium TARGET=SOC_C6678 LIBOS=LIB_RTOS
The executalbe blistestDSP.out is located in packages/ti/linalg/blis/testsuite.
